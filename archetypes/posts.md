---
draft: true
type: post
date: {{ .Date }}
lastmod: {{ .Date }}
publishDate: {{ .Date }}
title: {{ replace .Name "-" " " | title }}
description:
image:
  src:
  caption:
locale: id
tags:
  -
---
